import Tower from "./tower.js"

export default class TowerGroup extends Phaser.Physics.Arcade.Group {
    constructor(world, scene, data) {
        super(world, scene);
        this.data = data;

        this.scene.events.on('EnemyTileChange', () => this.onEnemyChange());
        this.scene.events.on('EnemyKill', () => this.onEnemyChange());
    }

    update(time, delta) {
        this.getChildren().forEach((tower) => tower.update(time, delta));
    }

    onEnemyChange() {
        // For loops since order matters.
        for (var tower of this.getChildren()) {
            var set = false;
            for (var enemy of this.scene.enemyGroup.getChildren()) {
                if (tower.isEnemyInRange(enemy)) {
                    tower.setTarget(enemy);
                    set = true;
                    break;
                }
            }
            if (!set) {
                tower.setTarget(null);
            }
        }
    }

    addTower(tileX, tileY, towerType) {
        let towerData;
        switch(towerType) {
            case 'yellowFlower':
            case 'redFlower':
            case 'blueFlower':
                towerData = this.data[towerType];
                break;
            default:
                window.alert('ERROR: ' + towerType);
        }
        const tower = new Tower(this.scene, tileX, tileY, towerData).setOrigin(0, 0);
        this.add(tower, true);
    }

    getTower(tileX, tileY) {
        const list = this.getChildren().filter(tower => tower.tileX == tileX && tower.tileY == tileY);
        return list.length > 0 ? list[0] : null;
    }

    removeTower(tileX, tileY) {
        const tower = this.getTower(tileX, tileY);
        if (tower != null) {
            this.removeTower(tower, true, true);
        }
    }
}