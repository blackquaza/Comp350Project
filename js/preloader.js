export default class Preloader extends Phaser.Scene {
    constructor() {
        super('Preloader');
    }

    preload() {
        // Load UI
        this.load.image('bottomBar', 'img/ui/bottomBar.png');
        this.load.image('tileset', 'img/tileset.png');
        this.load.json('mapData', 'json/maps/map1.json');
        this.load.spritesheet('button', 'img/ui/button.png', { frameWidth: 75 });
        this.load.image('sellIcon', 'img/ui/sell.png');

        // Load towers
        this.load.spritesheet('yellowFlowerSheet', 'img/towers/yellowFlower.png', { frameWidth: 50 });
        this.load.spritesheet('blueFlowerSheet', 'img/towers/blueFlower.png', { frameWidth: 50 });
        this.load.spritesheet('redFlowerSheet', 'img/towers/redFlower.png', { frameWidth: 50 });
        this.load.spritesheet('lightningSheet', 'img/towers/lightning.png', { frameWidth: 100, frameHeight: 10 });
        this.load.json('yellowFlowerData', 'json/towers/yellowFlower.json');
        this.load.json('redFlowerData', 'json/towers/redFlower.json');
        this.load.json('blueFlowerData', 'json/towers/blueFlower.json');

        // Load enemies
        this.load.spritesheet('mosquitoSheet', 'img/enemies/mosquito.png', { frameWidth: 50 });
        this.load.json('mosquitoData', 'json/enemies/mosquito.json');
    }

    create() {
        // Define tower animations
        this.anims.create({
            key: 'yellowFlowerAnim',
            frames: 'yellowFlowerSheet',
            frameRate: 2,
            repeat: -1
        });
        this.anims.create({
            key: 'blueFlowerAnim',
            frames: 'blueFlowerSheet',
            frameRate: 2,
            repeat: -1
        });
        this.anims.create({
            key: 'redFlowerAnim',
            frames: 'redFlowerSheet',
            frameRate: 2,
            repeat: -1
        });

        // Define lightning animations
        this.anims.create({
            key: 'yellowLightningAnim',
            frames: [0,1,2,3,4].map(num => ({key: 'lightningSheet', frame: num})),
            frameRate: 20,
            repeat: -1
        });
        this.anims.create({
            key: 'redLightningAnim',
            frames: [5,6,7,8,9].map(num => ({key: 'lightningSheet', frame: num})),
            frameRate: 20,
            repeat: -1
        });
        this.anims.create({
            key: 'blueLightningAnim',
            frames: [10,11,12,13, 14].map(num => ({key: 'lightningSheet', frame: num})),
            frameRate: 20,
            repeat: -1
        });

        // Define enemy animations
        this.anims.create({
            key: 'mosquitoAnim',
            frames: 'mosquitoSheet',
            frameRate: 6,
            repeat: -1
        });

        // Generate tile marker and range image.
        let g = this.add.graphics({fillStyle: { color: 0x7f5200, alpha: 0.5 } });
        g.fillRect(0, 0, 50, 50).generateTexture('tileMarkerAllowed');
        g.destroy();
        g = this.add.graphics({fillStyle: { color: 0xef0000, alpha: 0.5 } });
        g.fillRect(0, 0, 50, 50).generateTexture('tileMarkerDenied');
        g.destroy();
        g = this.add.graphics({fillStyle: { color: 0xefef00, alpha: 0.3 } });
        g.fillRect(0, 0, 50, 50).generateTexture('tileRange');
        g.destroy();

        // Generate selection image.
        g = this.add.graphics({fillStyle: { color: 0x0000ef, alpha: 0.7 } });
        g.fillRect(0, 0, 15, 3).fillRect(0, 0, 3, 15);
        g.fillRect(0, 35, 3, 15).fillRect(0, 47, 15, 3);
        g.fillRect(35, 0, 15, 3).fillRect(47, 0, 3, 15);
        g.fillRect(35, 47, 15, 3).fillRect(47, 35, 3, 15);
        g.generateTexture('tileSelected');
        g.destroy();

        // Generate upgrade arrows.
        g = this.add.graphics();
        g.lineStyle(1.5, 0x000000, 1.0).fillStyle(0x000000, 1.0);
        g.beginPath().arc(15, 15, 10, Phaser.Math.DegToRad(-90), Phaser.Math.DegToRad(-180), true).strokePath();
        g.fillTriangle(0, 15, 10, 15, 5, 20).generateTexture('upgradeArrowLeft');
        g.destroy();
        g = this.add.graphics();
        g.lineStyle(1.5, 0x000000, 1.0).fillStyle(0x000000, 1.0);
        g.beginPath().arc(15, 15, 10, Phaser.Math.DegToRad(-90), Phaser.Math.DegToRad(0)).strokePath();
        g.fillTriangle(20, 15, 30, 15, 25, 20).generateTexture('upgradeArrowRight');
        g.destroy();


        // Start!
        this.scene.start('Game');
    }
}