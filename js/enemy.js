export default class Enemy extends Phaser.Physics.Arcade.Sprite {
    constructor (scene, startY, json) {
        super(scene, -25, scene.tileToWorldY(startY)+25);

        this.play(json.anim);
        this.tileX = -1;
        this.tileY = startY;
        this.type = json.type;
        this.health = json.health;
        this.money = json.money;
        this.speed = Phaser.Math.GetSpeed(json.speed * 50, 1);

        this.facing;
        this.setFacing('right');
    }

    update(_time, delta) {
        const adjustedSpeed = this.speed * delta;
        switch (this.facing) {
            case 'right':
                this.x += adjustedSpeed;
                break;
            case 'down':
                this.y += adjustedSpeed;
                break;
            case 'left':
                this.x -= adjustedSpeed;
                break;
            case 'up':
                this.y -= adjustedSpeed;
                break;
        }

        const newTileX = this.getTileX();
        const newTileY = this.getTileY();
        if (newTileX != this.tileX || newTileY != this.tileY) {
            this.tileX = newTileX;
            this.tileY = newTileY;
            this.scene.events.emit('EnemyTileChange');
        }

        if (this.x % 50 == 25 && this.y % 50 == 25) { // On a tile.
            const tile = this.scene.ui.tileMap.getTileAt(tileX, tileY);
            const index = tile ? tile.index : -1;
            this.handleTile(index);
        }
    }

    getTileX() {
        return this.scene.worldToTileX(this.x);
    }

    getTileY() {
        return this.scene.worldToTileY(this.y);
    }

    setFacing(dir) {
        switch (dir) {
            case 'right':
                this.setAngle(0);
                break;
            case 'down':
                this.setAngle(90);
                break;
            case 'left':
                this.setAngle(180);
                break;
            case 'up':
                this.setAngle(-90);
                break;
        }
        this.facing = dir;
    }

    handleTile(index) {
        if (index == -1) {
            this.escape();
            return;
        } else if (index < 5) {
            this.setFacing(this.facing);
            return;
        }
        switch (this.facing) {
            case 'right':
                index == 5 ? this.setFacing('up') : index == 8 ? this.setFacing('down') : window.alert('ERROR: Invalid index ' + index);
                break;
            case 'down':
                index == 5 ? this.setFacing('left') : index == 6 ? this.setFacing('right') : window.alert('ERROR: Invalid index ' + index);
                break;
            case 'left':
                index == 6 ? this.setFacing('up') : index == 7 ? this.setFacing('down') : window.alert('ERROR: Invalid index ' + index);
                break;
            case 'up':
                index == 7 ? this.setFacing('right') : index == 8 ? this.setFacing('left') : window.alert('ERROR: Invalid index ' + index);
                break;
        }
    }

    hurt(damage) {
        this.health -= damage;
        if (this.health <= 0) {
            this.scene.enemyGroup.remove(this);
            this.kill();
        }
    }

    kill() {
        this.scene.addMoney(this.money, this.x, this.y - 20);
        this.scene.events.emit('EnemyKill');
        this.scene.time.addEvent({
            callback: () => this.setAlpha(this.alpha - 0.2),
            repeat: 5,
            delay: 100,
        });
        this.scene.time.addEvent({
            callback: () => this.destroy(true),
            delay: 600,
        });
    }

    escape() {
        //
    }
}