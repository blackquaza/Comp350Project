import RangeGroup from "./rangeGroup.js";

export default class Tower extends Phaser.Physics.Arcade.Sprite {
    constructor (scene, tileX, tileY, json) {
        super(scene, scene.tileToWorldX(tileX), scene.tileToWorldY(tileY));

        this.play(json.anim);
        this.tileX = tileX;
        this.tileY = tileY;
        this.type = json.type;
        this.damage = json.damage;
        this.frequency = json.frequency;
        this.upgrades = json.upgrades;
        this.cost = json.cost;
        this.sell = json.sell;

        this.target = null;
        this.timeEvent = null;
        this.range = JSON.parse(JSON.stringify(json.range)); // Quick deep clone.
        this.range.filter(tile => {
            tile.x += tileX;
            tile.y += tileY;
            return tile.x >= 0 && tile.x <= 15 && tile.y >= 0 && tile.y <= 8;
        });

        this.rangeGroup = new RangeGroup(scene.physics.world, scene, this.range);
        this.lightning = this.scene.add.sprite(this.x + 25, this.y + 25, json.lightningAnim)
            .setOrigin(0.05, 0.5).setAlpha(0).setDepth(1);
        this.lightning.play(json.lightningAnim);
    }

    update(_time, _delta) {
        if (this.target) {
            this.lightning.setAlpha(0.8);
            const angle = Phaser.Math.Angle.Between(this.lightning.x, this.lightning.y, this.target.x, this.target.y);
            const distance = Phaser.Math.Distance.Between(this.lightning.x, this.lightning.y, this.target.x, this.target.y)
            this.lightning.setRotation(angle);
            this.lightning.setDisplaySize(distance, 10)
        } else {
            this.lightning.setAlpha(0);
        }
    }

    setTileX(tileX) {
        this.setX(this.scene.tileToWorldX(tileX));
    }

    setTileY(tileY) {
        this.setY(this.scene.tileToWorldY(tileY));
    }

    isInRange(tileX, tileY) {
        return this.range.some(tile => tile.x == tileX && tile.y == tileY);
    }

    isEnemyInRange(enemy) {
        return this.isInRange(enemy.tileX, enemy.tileY);
    }

    setTarget(target) {
        if (target == this.target) {
            return;
        } else {
            this.target = target;
            this.onTargetChange();
        }
    }

    onTargetChange() {
        if (this.target && !this.timeEvent) {
            this.timeEvent = this.scene.time.addEvent({
                callback: () => this.target.hurt(this.damage),
                loop: true,
                delay: 1000.0 / this.frequency,
            });
        } else if (!this.target && this.timeEvent) {
            this.scene.time.removeEvent(this.timeEvent);
            this.timeEvent = null;
        }
    }

    showRange() {
        this.rangeGroup.show();
    }

    hideRange() {
        this.rangeGroup.hide();
    }

    hasUpgrades() {
        return this.upgrades.length > 0;
    }

    upgrade(choice) {
        const newTowerName = this.upgrades[choice];
        this.scene.addMoney(this.scene.towerData[newTowerName].cost * -1, this.x + 25, this.y);
        this.scene.towerGroup.addTower(this.tileX, this.tileY, newTowerName);
        const tileX = this.tileX;
        const tileY = this.tileY;
        this.scene.time.addEvent({
            callback: () => this.scene.selectTower(this.scene.towerGroup.getTower(tileX, tileY)),
            delay: 10,
        });
        this.scene.towerGroup.remove(this, true);
    }
}