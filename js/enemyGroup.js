import Enemy from "./enemy.js"

export default class EnemyGroup extends Phaser.Physics.Arcade.Group {
    constructor(world, scene, startY, data) {
        super(world, scene);
        this.startY = startY;
        this.data = data;
    }

    update(time, delta) {
        this.getChildren().forEach((enemy) => enemy.update(time, delta));
    }

    startWave(type, num) {
        this.scene.time.addEvent({
            callback: () => this.addEnemy(type),
            repeat: num - 1,
            delay: 500,
        })
    }

    addEnemy(enemyType) {
        let enemyData;
        switch(enemyType) {
            case 'mosquito':
                enemyData = this.data[enemyType];
                break;
            default:
                window.alert('ERROR: ' + enemyType);
        }
        const enemy = new Enemy(this.scene, this.startY, enemyData);
        this.add(enemy, true);
    }
}