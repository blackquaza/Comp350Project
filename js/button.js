export default class Button extends Phaser.Physics.Arcade.Image {
    constructor (scene, x, y, hotkey, hotkeyText, onClick, width=75, height=75, cost=0) {
        super(scene, x, y, 'button', 0);

        scene.add.existing(this);
        this.setOrigin(0, 0);
        this.setDisplaySize(width, height);
        this.disabled = false;
        this.locked = false;
        this.cost = cost;
        this.onClick = onClick;
        this.keyDown = false;
        this.hotkey = hotkey;
        this.hotkeyText = hotkeyText;
        this.sceneText = scene.add.text(x, y, hotkeyText, {fontSize: '12px', color: '#aaa', stroke: '#888', strokeThickness: 1});
        this.setInteractive();
        this.on('pointerover', () => this.onHover());
        this.on('pointerdown', () => this.onDown());
        this.on('pointerout', () => this.onOut());
        this.on('pointerup', () => this.onUp());
        scene.input.keyboard.on('keydown-'+hotkey, () => this.onKeyDown());
        scene.input.keyboard.on('keyup-'+hotkey, () => this.onKeyUp());
        scene.events.on('MoneyChange', (newVal) => this.onMoneyChange(newVal));
    }

    onHover() {
        if (!this.isDisabled()) {
            this.setFrame(1);
        }
    };

    onDown() {
        if (!this.isDisabled()) {
            this.setFrame(2);
        }
    }

    onOut() {
        if (!this.isDisabled()) {
            this.setFrame(0);
        }
    }

    onUp() {
        if (!this.isDisabled() && this.frame.name == 2) {
            this.setFrame(0);
            this.onClick();
        }
    }

    onKeyDown() {
        if (!this.isDisabled() && this.keyDown == false) {
            this.onClick();
            this.keyDown = true;
            this.setFrame(2);
            this.scene.time.addEvent({
                callback: () => this.setFrame(0),
                delay: 50,
            });
        }
    }

    onKeyUp() {
        if (!this.isDisabled()) {
            this.keyDown = false;
        }
    }

    onMoneyChange(newVal) {
        this.setDisabled(newVal < this.cost);
    }

    setAlpha(alpha) {
        this.sceneText.setAlpha(alpha);
        return super.setAlpha(alpha);
    }

    setCost(cost) {
        this.cost = cost;
        this.onMoneyChange(this.scene.money);
    }

    isDisabled() {
        return this.disabled || this.locked;
    }

    setLock(boolean) {
        this.locked = boolean;
    }

    setDisabled(boolean) {
        this.disabled = boolean;
        this.setFrame(boolean ? 3 : 0);
    }
}