export default class RangeGroup extends Phaser.Physics.Arcade.Group {
    constructor(world, scene, rangeList) {
        super(world, scene);
        rangeList.forEach((tile) => {
            const x = scene.tileToWorldX(tile.x)
            const y = scene.tileToWorldY(tile.y);
            const rangeTile = scene.add.image(x, y, 'tileRange').setOrigin(0, 0).setAlpha(0);
            this.add(rangeTile, true);
        })
    }

    show() {
        this.getChildren().forEach((tile) => tile.setAlpha(1));
    }

    hide() {
        this.getChildren().forEach((tile) => tile.setAlpha(0));
    }
}