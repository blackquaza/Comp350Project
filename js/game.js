import Button from "./button.js";
import EnemyGroup from "./enemyGroup.js";
import Tower from "./tower.js";
import TowerGroup from "./towerGroup.js";

export default class Game extends Phaser.Scene {
    constructor() {
        super('Game');

        this.money = 0;
        this.moneyText;
        this.towerGroup;
        this.enemyGroup;
        this.towerData;
        this.ui = {
            tileMap: null,
            tileMapMarker: null,
            tileSelectMarker: null,
            selectedTower: null,
            selectedImageSet: {},
            tower1: null,
            tower2: null,
            tower3: null,
            tower4: null,
            upgrade1: null,
            upgrade2: null,
            upgradeImageSet: {},
            upgradeArrowLeft: null,
            upgradeArrowRight: null, 
            start: null,
            sell: null,
            cursor: null,
            cursorTower: null,
            cursorTowerSet: {},
            isKeyDown: false,
        };
    }

    create() {
        // Get and show tilemap
        const mapData = this.cache.json.get('mapData');
        this.ui.tileMap = this.make.tilemap({ data:mapData.tileArray, tileWidth: 50, tileHeight: 50});
        this.ui.tileMap.addTilesetImage("tileset");
        const layer = this.ui.tileMap.createLayer(0, "tileset");

        // Show grid and mouseover
        this.add.grid(0, 0, 800, 452, 50, 50, '#000', 0, '#000', 0.2).setOrigin(0, 0);
        this.ui.tileMapMarker = this.add.image(0, 0, 'tileMarkerAllowed').setOrigin(0, 0);
        this.ui.tileSelectMarker = this.add.image(0, 0, 'tileSelected').setOrigin(0, 0).setAlpha(0);
        this.setTileMarkerPlaceable(true);

        // User interactions.
        layer.setInteractive();
        layer.on('pointerover', (_pointer, x, y) => this.pointerOver(x, y));
        layer.on('pointermove', (_pointer, x, y) => this.pointerMove(x, y));
        layer.on('pointerout', () => this.pointerOut());
        layer.on('pointerup', (_pointer, x, y) => this.pointerUp(x, y));
        this.input.keyboard.on('keydown-ESC', () => this.keyDown());
        this.input.keyboard.on('keyup-ESC', () => this.keyUp());

        // Add groups.
        this.enemyData = {
            mosquito: this.cache.json.get('mosquitoData'),
        }
        this.enemyGroup = new EnemyGroup(this.physics.world, this, mapData.startY, this.enemyData);
        this.towerData = {
            yellowFlower: this.cache.json.get('yellowFlowerData'),
            redFlower: this.cache.json.get('redFlowerData'),
            blueFlower: this.cache.json.get('blueFlowerData'),
        }
        this.towerGroup = new TowerGroup(this.physics.world, this, this.towerData);

        // Add bottom bar
        this.add.image(0, 440, 'bottomBar').setOrigin(0, 0);
        this.moneyText = this.add.text(100, 495, '', {
            fontSize: '22px', color: '#000', stroke: '#333', strokeThickness: 2
        }).setOrigin(0.5, 0.5);
        this.ui.tower1 = new Button(this, 245, 475, 'ONE', '1', () => this.setCursor('yellowFlower'), 50, 50, this.towerData.yellowFlower.cost);
        this.add.image(245, 475, 'yellowFlowerSheet', 0).setOrigin(0, 0).setAlpha(0.5);
        this.ui.tower2 = new Button(this, 305, 475, 'TWO', '2', () => this.setCursor('yellowFlower'), 50, 50, 9000);
        this.add.image(305, 475, 'yellowFlowerSheet', 0).setOrigin(0, 0).setAlpha(0.5);
        this.ui.tower3 = new Button(this, 245, 535, 'THREE', '3', () => this.setCursor('yellowFlower'), 50, 50, 9000);
        this.add.image(245, 535, 'yellowFlowerSheet', 0).setOrigin(0, 0).setAlpha(0.5);
        this.ui.tower4 = new Button(this, 305, 535, 'FOUR', '4', () => this.setCursor('yellowFlower'), 50, 50, 9000);
        this.add.image(305, 535, 'yellowFlowerSheet', 0).setOrigin(0, 0).setAlpha(0.5);
        this.ui.selectedImageSet = {
            yellowFlower: this.add.image(475, 475, 'yellowFlowerSheet', 0).setOrigin(0, 0).setAlpha(0),
            redFlower: this.add.image(475, 475, 'redFlowerSheet', 0).setOrigin(0, 0).setAlpha(0),
            blueFlower: this.add.image(475, 475, 'blueFlowerSheet', 0).setOrigin(0, 0).setAlpha(0),
        }
        this.ui.upgrade1 = new Button(this, 445, 540, 'Q', 'Q', () => this.ui.selectedTower.upgrade(0), 50, 50);
        this.ui.upgrade1.setAlpha(0).setLock(true)
        this.ui.upgrade2 = new Button(this, 505, 540, 'E', 'E', () => this.ui.selectedTower.upgrade(1), 50, 50);
        this.ui.upgrade2.setAlpha(0).setLock(true)
        this.ui.upgradeImageSet = {
            yellowFlower0: this.add.image(445, 540, 'redFlowerSheet', 0).setOrigin(0, 0).setAlpha(0),
            yellowFlower1: this.add.image(505, 540, 'blueFlowerSheet', 0).setOrigin(0, 0).setAlpha(0),
        }
        this.ui.upgradeArrowLeft = this.add.image(460, 495, 'upgradeArrowLeft').setOrigin(0, 0).setAlpha(0);
        this.ui.upgradeArrowRight = this.add.image(510, 495, 'upgradeArrowRight').setOrigin(0, 0).setAlpha(0);
        this.add.image(680, 495, 'mosquitoSheet', 0);
        this.add.text(720, 495, 'x3', {
            fontSize: '16px', color: '#333', stroke: '#333', strokeThickness: 1
        }).setOrigin(0.5, 0.5);
        this.ui.start = new Button(this, 625, 535, 'SPACE', '', () => this.enemyGroup.startWave('mosquito', 3), 150, 50);
        this.add.text(700, 560, 'START WAVE', {
            fontSize: '18px', color: '#540', stroke: '#333', strokeThickness: 1
        }).setOrigin(0.5, 0.5);
        this.ui.sell = new Button(this, 80, 540, 'S', 'S', () => this.setCursor('sell'), 40, 40);
        this.add.image(100, 560, 'sellIcon').setDisplaySize(35, 35);

        // Set up cursor
        this.input.setDefaultCursor('url(img/ui/cursor.png), pointer');
        this.ui.cursorTowerSet = {
            yellowFlower: new Tower(this, 1, 1, this.towerData.yellowFlower).setOrigin(0, 0).setAlpha(0),
        }
        Object.keys(this.ui.cursorTowerSet).forEach((towerName) => {
            this.add.existing(this.ui.cursorTowerSet[towerName]);
        });

        // Variable init
        this.setMoney(900);
        this.setCursor('default');
    }

    update(time, delta) {
        this.enemyGroup.update(time, delta);
        this.towerGroup.update(time, delta);
    }

    setTileMarkerPlaceable(boolean) {
        this.ui.tileMapMarker.setTexture(boolean ? 'tileMarkerAllowed' : 'tileMarkerDenied');
    }

    getPlaceable(tileX, tileY) {
        const tile = this.ui.tileMap.getTileAt(tileX, tileY);
        const tower = this.towerGroup.getTower(tileX, tileY);
        return tile ? tile.index == 0 && !tower : false;
    }

    pointerOver(x, y) {
        this.ui.tileMapMarker.setAlpha(1);
        if (this.ui.cursorTower != null) {
            this.ui.cursorTower.setAlpha(0.5);
        }
        this.pointerMove(x, y)
    }

    pointerMove(x, y) {
        const tileX = this.worldToTileX(x);
        const tileY = this.worldToTileY(y);
        const worldX = this.tileToWorldX(tileX);
        const worldY = this.tileToWorldY(tileY)
        this.ui.tileMapMarker.setX(worldX);
        this.ui.tileMapMarker.setY(worldY);
        const placeable = this.getPlaceable(tileX, tileY);
        this.setTileMarkerPlaceable(placeable);
        if (this.ui.cursorTower != null) {
            this.ui.cursorTower.setX(worldX);
            this.ui.cursorTower.setY(worldY);
            if (!placeable) {
                this.ui.cursorTower.setAlpha(0.2);
            } else {
                this.ui.cursorTower.setAlpha(0.5);
            }
        }
    }

    pointerOut() {
        this.ui.tileMapMarker.setAlpha(0);
        if (this.ui.cursorTower != null) {
            this.ui.cursorTower.setAlpha(0);
        }
    }

    pointerUp(x, y) {
        this.onClick(this.worldToTileX(x), this.worldToTileY(y));
    }

    keyDown() {
        if (!this.ui.isKeyDown) {
            this.onEscape();
        }
        this.ui.isKeyDown = true;
    }

    keyUp() {
        this.ui.isKeyDown = false;
    }

    worldToTileX(x) {
        return this.ui.tileMap.worldToTileX(x);
    }

    worldToTileY(y) {
        return this.ui.tileMap.worldToTileY(y);
    }

    tileToWorldX(tileX) {
        return this.ui.tileMap.tileToWorldX(tileX);
    }

    tileToWorldY(tileY) {
        return this.ui.tileMap.tileToWorldY(tileY);
    }

    selectTower(tower) {
        if (this.ui.selectedTower) {
            this.ui.selectedTower.hideRange();
            this.ui.selectedImageSet[this.ui.selectedTower.type].setAlpha(0);
            if (this.ui.selectedTower.hasUpgrades()) {
                this.ui.upgrade1.setAlpha(0).setLock(true);
                this.ui.upgrade2.setAlpha(0).setLock(true);
                this.ui.upgradeImageSet[this.ui.selectedTower.type + '0'].setAlpha(0);
                this.ui.upgradeImageSet[this.ui.selectedTower.type + '1'].setAlpha(0);
                this.ui.upgradeArrowLeft.setAlpha(0);
                this.ui.upgradeArrowRight.setAlpha(0);
            }
        }
        if (tower) {
            tower.showRange();
            this.ui.selectedImageSet[tower.type].setAlpha(0.8);
            this.ui.tileSelectMarker.setAlpha(1);
            this.ui.tileSelectMarker.setX(this.tileToWorldX(tower.tileX));
            this.ui.tileSelectMarker.setY(this.tileToWorldY(tower.tileY));
            if (tower.hasUpgrades()) {
                this.ui.upgrade1.setAlpha(1).setLock(false);
                this.ui.upgrade2.setAlpha(1).setLock(false);
                this.ui.upgrade1.setCost(this.towerData[tower.upgrades[0]].cost);
                this.ui.upgrade2.setCost(this.towerData[tower.upgrades[1]].cost);
                this.ui.upgradeImageSet[tower.type + '0'].setAlpha(1);
                this.ui.upgradeImageSet[tower.type + '1'].setAlpha(1);
                this.ui.upgradeArrowLeft.setAlpha(1);
                this.ui.upgradeArrowRight.setAlpha(1);
            }
        } else {
            this.ui.tileSelectMarker.setAlpha(0);
        }
        this.ui.selectedTower = tower;
        
    }

    setCursor(type) {
        if (this.ui.cursor == type) {
            return;
        }
        this.ui.cursor = type;
        let cursor;
        switch (type) {
            case 'default':
                cursor = 'url(img/ui/cursor.png), pointer';
                break;
            case 'sell':
                cursor = 'url(img/ui/cursorSell.png), pointer';
                break;
            case 'yellowFlower':
                cursor = 'url(img/ui/cursor.png), pointer';
                this.ui.cursorTower = this.ui.cursorTowerSet[type];
                this.ui.cursorTower.setAlpha(this.ui.tileMapMarker.alpha / 2.0);
                this.ui.cursorTower.setX(this.ui.tileMapMarker.x);
                this.ui.cursorTower.setY(this.ui.tileMapMarker.y);
                break;
        }
        this.input.manager.canvas.style.cursor = cursor;
        this.input.setDefaultCursor(cursor);
    }

    onClick(tileX, tileY) {
        switch (this.ui.cursor) {
            case 'default':
                this.selectTower(this.towerGroup.getTower(tileX, tileY));
                break;
            case 'sell':
                const tower = this.towerGroup.getTower(tileX, tileY);
                if (tower) {
                    this.addMoney(tower.sell, tower.x + 25, tower.y);
                    this.towerGroup.remove(tower, true, true);
                    this.onEscape();
                }
                break;
            case 'yellowFlower':
                if (this.getPlaceable(tileX, tileY)) {
                    this.towerGroup.addTower(tileX, tileY, this.ui.cursor);
                    this.addMoney(this.ui.cursorTower.cost * -1, this.ui.cursorTower.x + 25, this.ui.cursorTower.y);
                    this.onEscape();
                }
                break;
        }
    }

    onEscape() {
        if (this.ui.cursorTower) {
            this.ui.cursorTower.setAlpha(0);
        }
        this.ui.cursorTower = null;
        this.setCursor('default');
        this.selectTower(null);
    }

    addMoney(addVal, x, y) {
        const text = this.add.text(x, y, addVal > 0 ? '+' + addVal : addVal, {fontSize: '20px', color: '#000', stroke: '#333', strokeThickness: 3});
        text.setOrigin(0.5, 0);
        this.time.addEvent({
            callback: () => {text.y -= 0.5; text.alpha -= 0.04},
            repeat: 24,
            delay: 75,
        });
        this.time.addEvent({
            callback: () => text.destroy(true),
            delay: 2000,
        });
        return this.setMoney(this.money + addVal);
    }

    setMoney(newVal) {
        if (this.money != newVal) {
            this.money = newVal;
            this.moneyText.setText(this.getMoneyText());
            this.events.emit('MoneyChange', newVal);
        }
        return this.money;
    }

    getMoneyText() {
        if (this.money < 10000) {
            return this.money.toString();
        } else if (this.money < 10000000) {
            return (Math.floor(this.money / 100)/10).toFixed(1) + " K";
        } else {
            return (Math.floor(this.money / 100000)/10).toFixed(1) + " M";
        }
    }
}