import Preloader from './preloader.js';
import Game from './game.js';

const main = () => {
    const config = {
        type: Phaser.AUTO,
        width: 800,
        height: 600,
        backgroundColor: '#eee',
        scene: [ Preloader, Game ],
        physics: {
            default: 'arcade',
            arcade: { debug: false }
        }
    };
    
    new Phaser.Game(config);

}

main();
